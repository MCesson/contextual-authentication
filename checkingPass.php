<?php session_start();

if (!isset($_SESSION['authenticatedUser'])) {

    if (isset($_SESSION['tempID'])) {

        require("functions/checkPassFunction.php");

        if (passwordMatchsId("pass", $_POST['password'])) {
            $_SESSION['passwordChecked'] = true;
            header("Location: checkingContext.php");

        } else {
            unset($_SESSION['tempID']);
            header("Location: loginId.php?formError");
        }

    } else { // users trying to reach this page without having done previous steps 
        header("Location: loginId.php");
    }

} else { // authenticated users don't have to login
	header("Location: .");
}

?>