<?php session_start();

if (!isset($_SESSION['authenticatedUser'])) {

	switch (true) { // check whether there are bad formatted data

		case !isset($_POST['username']) || empty($_POST['username']):
			header("Location: register.php?formError=name");
			break;

		case !isset($_POST['password']) || empty($_POST['password']):
			header("Location: register.php?formError=password");
			break;

		case !isset($_POST['password2']) || empty($_POST['password2']):
			header("Location: register.php?formError=password2");
			break;

		case $_POST['password'] <> $_POST['password2']:
			header("Location: register.php?formError=passwordsdontmatch");
			break;
		
		default:

			// there are no bad formated data: let's write into the db
			require_once("functions/dbFunctions.php");
			
			$db = connectDB();
			$otbc = bin2hex(random_bytes(5));

			if ($db !== null) {

				do { // generate a suitable id
					$id = "2021" . random_int(1000,9999);
					$selectQuery = $db->prepare('SELECT * FROM Users WHERE id = ?');
					$selectQuery->execute(array($id));
				} while ($selectQuery->fetch());

				$selectQuery->closeCursor();				

				$insertQuery = $db->prepare("INSERT INTO Users (id, username, pass, otbc)
											VALUES (:id, :username, :pass, :otbc)");
				
				try {
					$insertQuery->execute(array(
						'id' => $id,
						'username' => $_POST['username'],
						'pass' => password_hash($_POST['password'], PASSWORD_DEFAULT),
						'otbc' => password_hash($otbc, PASSWORD_DEFAULT)
					));
				} catch (Exception $e) {
					$error = $e->getMessage();
				}

				$insertQuery->closeCursor();
				$db = null; // 'disconnect' database

			} else {
				$error = "No connection was established with the db";
			}
			
			if (!isset($error)) {
	?>

				<h1>Registration works!</h1>
				<br />

				<p>
					Here is your personnal ID : <?php echo $id; ?>
					<br />
					Here is your one-time backup code : <?php echo $otbc; ?>
					<br />
					Please remember it or write it down since you will need it to login.
				</p>

				<button onclick="window.location.href='loginId.php'">Login</button>

	<?php
			} else { // isset($error) -> echo $error for more info
	?>

				<h1>Something went wrong...</h1>

				<p>
					Please retry soon. 
					<br />
					If nothing works since several tries, try to contact an administrator.
				</p>

				<button onclick="window.location.href='register.php'">Retry</button>
			
	<?php
			}
			break;
	}

} else { // authenticated users don't have to register
	header("Location: .");
} 

?>