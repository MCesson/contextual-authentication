<?php session_start(); ?>
<!DOCTYPE html>
<html>

<head>
	<title>Register</title>
	<meta charset="utf-8">
</head>

<body>
	<main>
	<?php if(!isset($_SESSION['authenticatedUser'])) { ?>

		<h1>Please register using the form bellow</h1>

		<form id="register" method="POST" action="registering.php">

			<label for="username">Name</label>
			<br />
			<input type="text" name="username" id="username" />
			<br /><br />
			
			<label for="password">Password</label>
			<br />
			<input type="password" name="password" id="password" />
			<br /><br />

			<label for="password2">Confirm password</label>
			<br />
			<input type="password" name="password2" id="password2" />
			<br /><br />

			<?php 
			if (isset($_GET['formError'])) { // in case of a problem occurs
				echo "An error occurs, be sure to complete all fields and that both passwords are the same.";
				echo "<br />";
			}
			?>

			<input type="submit" value="Register" />
		</form>

		<br />

		<div>	
			<p>Come back to home page <a href=".">here</a>.</p>
		</div>

	<?php } else { // authenticated users don't have to register
		header("Location: .");
	} ?>

	</main>
</body>

</html>
