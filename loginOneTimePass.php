<?php session_start(); ?>
<!DOCTYPE html>
<html>

<head>
	<title>Recover password</title>
	<meta charset="utf-8">
</head>

<body>
	<main>
	<?php if(!isset($_SESSION['authenticatedUser'])) { ?>

		<h1>Please enter your credentials bellow</h1>

		<form id="recover" method="POST" action="checkingOneTimePass.php">
			
			<label for="otbc">One-Time Backup Code</label>
			<br />
			<input type="password" name="otbc" id="otbc" />
			<br /><br />

			<input type="submit" value="Next" name="loginOneTimePass" />
		</form>
		
		<br />
		
		<button onclick="window.location.href='loginPass.php'">Go Back</button>

	<?php } else { // authenticated users don't have to login
		header("Location: .");
	} ?>

	</main>
</body>

</html>