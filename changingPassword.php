<?php session_start();

if (isset($_SESSION['authenticatedUser']) || isset($_SESSION['codeChecked'])) {

	switch (true) { // check whether there are bad formatted data

		case !isset($_POST['password']) || empty($_POST['password']):
			header("Location: register.php?formError=password");
			break;

        case !isset($_POST['password2']) || empty($_POST['password2']):
            header("Location: register.php?formError=password2");
            break;

		case $_POST['password'] <> $_POST['password2']:
			header("Location: register.php?formError=passwordsdontmatch");
			break;
		
		default:

			// there are no bad formated data: let's write into the db
			require_once("functions/dbFunctions.php");

			// a bit of ternary: match the use case (authenticated user || password recovering)
			$id = (isset($_SESSION['codeChecked']))? $_SESSION['tempID'] : $_SESSION['authenticatedUser']['id'];
            $otbc = bin2hex(random_bytes(5));

            $db = connectDB();

			if ($db !== null) {			

				$updateQuery = $db->prepare(
					"UPDATE Users
					SET pass = :pass, otbc = :otbc
					WHERE id = :id"
				);
				
				try {
					$updateQuery->execute(array(
						'id' => $id,
						'pass' => password_hash($_POST['password'], PASSWORD_DEFAULT),
						'otbc' => password_hash($otbc, PASSWORD_DEFAULT)
					));
				} catch (Exception $e) {
					$error = $e->getMessage();
				}

				$updateQuery->closeCursor();
				$db = null; // 'disconnect' database

			} else {
				$error = "No connection was established with the db";
			}
			
			if (!isset($error)) {

				session_destroy(); // everybody is logged out after password change
	?>

				<h1>Password updated!</h1>
				<br />

				<p>
					Here is your new one-time backup code : <?php echo $otbc; ?>
					<br />
					Please remember write it down somewhere since you will need it if you loose your password.
				</p>

				<button onclick="window.location.href='loginId.php'">Login</button>

	<?php
			} else { // isset($error) -> echo $error for more info
	?>

				<h1>Something went wrong...</h1>

				<p>
					Please retry soon. 
					<br />
					If nothing works since several tries, try to contact an administrator.
				</p>

				<button onclick="window.location.href='changePassword.php'">Retry</button>
			
	<?php
			}
			break;
	}

} else { // not authenticated users + users not recovering their password
	header("Location: .");
} 

?>