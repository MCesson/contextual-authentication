<?php

require_once("functions/dbFunctions.php");

function getUserData($idUser) {
// return an array of all registered and needed information for one given user

    $userData = array();
    $db = connectDB();

    if ($db !== null) {

        $selectQuery = $db->prepare('SELECT * FROM Users WHERE id = ?');
        $selectQuery->execute(array($idUser));

        if($dbFirstRow = $selectQuery->fetch()) {

            // match names in db and names from code
            $userData['id'] = $dbFirstRow['id'];
            $userData['username'] = $dbFirstRow['username'];
        }

        $selectQuery->closeCursor();
    }

    $db = null;
    return $userData; // if it is empty, something went wrong
}


function getContextualRequierements($idUser) {
// return an array of all registered and needed contextual requierements for one given user

    $contextualRequierements = array();
    $db = connectDB();

    if ($db !== null) {

        $selectQuery = $db->prepare('SELECT * FROM Contexts WHERE idUser = ?');
        $selectQuery->execute(array($idUser));

        $i = 0;
        while($dbRow = $selectQuery->fetch()) {

            // match names in db and names from code
            $contextualRequierements[$i]['IPaddress'] = $dbRow['ipAddress'];
            $contextualRequierements[$i]['Location'] = $dbRow['deviceLocation'];
            $contextualRequierements[$i]['Time'] = '';
            $contextualRequierements[$i]['Browser'] = $dbRow['browserName'];
            $contextualRequierements[$i]['OS'] = $dbRow['osName'];

            $i += 1;
        }

        $selectQuery->closeCursor();
    }

    $db = null;
    return $contextualRequierements; // if it is empty, something went wrong
}


function getBrowser($userAgent) { // return browser name

    if(preg_match('/MSIE/i',$userAgent) && !preg_match('/Opera/i',$userAgent)) {
        return "MSIE";
    
    } elseif(preg_match('/Firefox/i',$userAgent)) {
        return "Firefox";

    } elseif(preg_match('/Chrome/i',$userAgent)) {
        return "Chrome";

    } elseif(preg_match('/Safari/i',$userAgent)) {
        return "Safari";

    } elseif(preg_match('/Opera/i',$userAgent)) {
        return "Opera";

    } else {
        return "Unknown";
    }  
}


function getOS($userAgent) { // return OS name

    if (preg_match('/linux/i', $userAgent)) {
        return 'Linux';

    } elseif (preg_match('/macintosh|mac os x/i', $userAgent)) {
        return 'Mac';

    } elseif (preg_match('/windows|win32/i', $userAgent)) {
        return 'Windows';

    } else {
        return 'Unknown';
    }
}


function meetingRequierements() {

    $contextualRequierements = getContextualRequierements($_SESSION['tempID']);

    print_r($_SERVER['HTTP_USER_AGENT']);
    echo "<br /><br />";

    $contextualClientData = array(
        'IPaddress' => $_SERVER['REMOTE_ADDR'],
        'Location' => '', // add a way to get it
        'Time' => '', // think about how to handle it
        'Browser' => getBrowser($_SERVER['HTTP_USER_AGENT']),
        'OS' => getOS($_SERVER['HTTP_USER_AGENT'])
    );

    // what occurs if $contextualRequierements is empty ??
    if (!empty($contextualRequierements)) {

        foreach ($contextualRequierements[0] as $key => $value) {

            // if no requirement is given, any contextual data from the client is fine
            if ($value !== '' && $contextualClientData[$key] !== $value) {
                var_dump($contextualClientData[$key]);
                $notMeetingRequierements = true;
            }
        }

    } else { // $_SESSION['tempID'] doesn't belong to table `Contexts` in DB
        $notMeetingRequierements = true; // -> should not be authenticated
    }

    return !isset($notMeetingRequierements);
}

?>