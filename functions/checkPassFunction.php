<?php

require_once("functions/dbFunctions.php");

function passwordMatchsId($typeOfPass, $pass) {

    $db = connectDB();

    if ($db !== null) {

        //not working with 'SELECT :columnName FROM....' / good news: $typeOfPass isn't a user input
        $selectQuery = $db->prepare('SELECT ' . $typeOfPass . ' FROM Users WHERE id = ?');
        $selectQuery->execute(array($_SESSION['tempID']));

        if($hash = $selectQuery->fetch()) { // there is a result
            if (password_verify($pass, $hash[0])) {
                $passwordMatchsId = true;
            }
        }
        $selectQuery->closeCursor();
    }
    $db = null; // 'disconnect' database
    return $passwordMatchsId;
}

?>