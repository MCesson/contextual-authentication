<?php 

function connectDB() {
  // adapt the coordinates of the database used + disable errors printing before going prod

  try {
    $db = new PDO('mysql:host=localhost;dbname=dbContextualAuthentication;charset=utf8', 
          'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)); 
    return $db;

  } catch (Exception $e) { // website have to work without database connexion
    echo 'Troubles while connecting to the database<br />';
    echo $e->getMessage();
    return null;
  }
}

/* minimal code :
require_once("functions/dbFunctions.php");
$db = connectDB();
if ($db !== null) {
  // do things
}
$db = null; // 'disconnect' database */

?>