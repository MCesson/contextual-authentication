<?php session_start();

if (isset($_SESSION['authenticatedUser'])) {

    require_once("functions/dbFunctions.php");
    
    $db = connectDB();

    if ($db !== null) {				

        $deleteQuery = $db->prepare("DELETE FROM Users WHERE id = ?");
        
        try {
            $deleteQuery->execute(array($_SESSION['authenticatedUser']['id']));
        } catch (Exception $e) {
            $error = $e->getMessage();
        }

        $deleteQuery->closeCursor();
        $db = null; // 'disconnect' database

    } else {
        $error = "No connection was established with the db";
    }
    
    if (!isset($error)) { // account deleted
        
        session_destroy();
        header("Location: .");

    } else { // isset($error) -> echo $error for more info
?>

        <h1>Something went wrong...</h1>

        <p>
            Please retry soon. 
            <br />
            If nothing works since several tries, try to contact an administrator.
        </p>

        <button onclick="window.location.href='deletingAccount.php'">Retry account deletion</button>
    
<?php
    }

} else { // not authenticated users cannot delete any account
	header("Location: .");
} 

?>