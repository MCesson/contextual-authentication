<?php session_start(); ?>
<!DOCTYPE html>
<html>

<head>
    <title>Contextual Authentication</title>
    <meta charset="utf-8">
</head>

<body>
    <main>
        <?php
        if(isset($_SESSION['authenticatedUser'])){
        ?>
            <h1>Hello guys who already login !</h1>
            <button onclick="window.location.href='loggingout.php'">Logout</button>
            <button onclick="window.location.href='changePassword.php'">Change password</button>
            <button onclick="window.location.href='deleteAccount.php'">Delete account</button>
        <?php
        } else {
        ?>
            <button onclick="window.location.href='loginId.php'">Login</button>
            <button onclick="window.location.href='register.php'">Register</button>
        <?php
        }
        ?>
    </main>
</body>

</html>