<?php session_start(); ?>
<!DOCTYPE html>
<html>

<head>
	<title>Delete account</title>
	<meta charset="utf-8">
</head>

<body>
	<main>
	<?php if(isset($_SESSION['authenticatedUser'])) { ?>

		<h1>Delete your account</h1>
        <br />

        <p>
            Please be aware that if you hit the 'delete' button again, your account
            and all related data will be permanently removed.
            <br />
            We won't be able in any way to help you recovering your data.
        </p>

        <button onclick="window.location.href='.'">Go back</button>
        <button onclick="window.location.href='deletingAccount.php'">Permanent delete</button>

	<?php } else { // not authenticated users cannot delete any account
		header("Location: .");
	} ?>

	</main>
</body>

</html>
