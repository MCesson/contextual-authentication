<?php session_start();

if (!isset($_SESSION['authenticatedUser'])) {
    $_SESSION['tempID'] = $_POST['id'];
    header("Location: loginPass.php");

} else { // authenticated users don't have to login
	header("Location: .");
} 

?>