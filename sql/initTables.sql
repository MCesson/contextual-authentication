/* commandline version : SOURCE path/initTables.sql */
USE dbContextualAuthentication;

CREATE TABLE IF NOT EXISTS Users (
	id INT PRIMARY KEY NOT NULL,
	username VARCHAR(30) NOT NULL,
	pass VARCHAR(255) NOT NULL,
	otbc VARCHAR(255) NOT NULL
);

DROP TABLE IF EXISTS Contexts;

CREATE TABLE IF NOT EXISTS Contexts (
	ipAddress VARCHAR(45),
	deviceLocation VARCHAR(30),
	browserName VARCHAR(30),
	osName VARCHAR(30),
	idUser INT NOT NULL,
	FOREIGN KEY (idUser) REFERENCES Users(id) ON DELETE CASCADE
);