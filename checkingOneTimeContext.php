<?php session_start();

if (!isset($_SESSION['authenticatedUser'])) {

    if (isset($_SESSION['tempID']) && isset($_SESSION['codeChecked'])) {

        require("functions/checkContextFunctions.php");

        if (meetingRequierements()) { // all is good, ask for a new password
            header("Location: changePassword.php");

        } else { // not meeting contextual requierements

            unset($_SESSION['tempID']);
            unset($_SESSION['codeChecked']);
?>

            <h1>Authentication forbidden</h1>

            <p>
                You're not meeting the requierements for contextual authentication. 
                <br />
                If you belong to the company, please contact an administrator to update your contextual data.
            </p>
        
<?php
        }  
    
    } else { // users trying to reach this page without having done previous steps 
        
        unset($_SESSION['tempID']);
        unset($_SESSION['codeChecked']);
        header("Location: loginId.php");
    }

} else { // authenticated users don't have to login
	header("Location: .");
}

?>