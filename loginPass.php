<?php session_start(); ?>
<!DOCTYPE html>
<html>

<head>
	<title>Login</title>
	<meta charset="utf-8">
</head>

<body>
	<main>
	<?php if(!isset($_SESSION['authenticatedUser'])) { ?>

		<h1>Please enter your credentials bellow</h1>

		<form id="login" method="POST" action="checkingPass.php">
			
			<label for="password">Password</label>
			<br />
			<input type="password" name="password" id="password" />
			<br /><br />

			<input type="submit" value="Next" name="loginPass" />
		</form>
		
		<br />
		
		<button onclick="window.location.href='loginId.php'">Go Back</button>
		<button onclick="window.location.href='loginOneTimePass.php'">Reset Password</button>

	<?php } else { // authenticated users don't have to login
		header("Location: .");
	} ?>

	</main>
</body>

</html>