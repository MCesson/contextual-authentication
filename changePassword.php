<?php session_start(); ?>
<!DOCTYPE html>
<html>

<head>
	<title>Change password</title>
	<meta charset="utf-8">
</head>

<body>
	<main>
	<?php if(isset($_SESSION['authenticatedUser']) || isset($_SESSION['codeChecked'])) { ?>

		<h1>Please change your password using the form bellow</h1>

		<form id="changePassword" method="POST" action="changingPassword.php">

			<label for="password">Password</label>
			<br />
			<input type="password" name="password" id="password" />
			<br /><br />

			<label for="password2">Confirm password</label>
			<br />
			<input type="password" name="password2" id="password2" />
			<br /><br />

			<?php 
			if (isset($_GET['formError'])) { // in case of a problem occurs
				echo "An error occurs, be sure to complete all fields and that both passwords are the same.";
				echo "<br />";
			}
			?>

			<input type="submit" value="Update" />
		</form>

		<br />

	<?php } else { // not authenticated users + users not recovering their password
		header("Location: .");
	} ?>

	</main>
</body>

</html>
