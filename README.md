# Contextual Authentication

Feel free to clone the project, hijack it, make new releases or anything else you want ;)


## Testing environment

**Xampp**: give a look to htdocs/ directory in order to run the website.  
**phpMyAdmin**: import files from this sql/ directory to set up your testing MySQL database under Xampp.


## Testing the current contextual authentication

For now, context has to be set up in the database by hand (user cannot register themselves, only administrators can thus register users' context).  
So here are some steps to test contextual authentication as it is currently:

1. Create an account as previously (keep your ID somewhere).
2. Browse the page whatIsMyContext.php to get some of your current contextual information.
3. Thanks to the data you got, edit manually the table 'Contexts' in the DB to match your context and your ID -> now if you try to authenticate, it should work.
4. Edit again the table 'Contexts' but altering the line you've just created with some random context data -> if you try to authenticate again, it should not work.


## Giving a look on how pages are related to each others

![Simplified diagram of website connections](relationsDiagram.svg)

* Square-shaped boxes stand for homepages
* Redirections from a box to the same box due to bad formatted data form are not drawn
* Redirections on *index.php* boxes for not authorised users on each pages are not drawn neither

The order of pages should be relatively easy to modify, looking for blocks of codes containing 'header("Location: ???")' lines, 'form', 'button' or 'a' tags' in pages you want to modify the order.  
For instance to alter the login process, contextual checks can take place before dealing with passwords and so, users are asked for their passwords only if they don't meet the requierements. 


## Adding new contextual data

Regarding new requierements:

* Edit the database's structure with the file *initTables.sql*
* Add some new data into your fresh 'Contexts' table
* Edit matching names of function *getContextualRequierements* in file *checkContextFunctions.php*

Regarding new way of collecting contextual data from client:

* Edit the array *$contextualClientData* from the file *checkContextFunctions.php*


***Have fun!***