<?php session_start(); ?>
<!DOCTYPE html>
<html>

<head>
	<title>Login</title>
	<meta charset="utf-8">
</head>

<body>
	<main>
	<?php if(!isset($_SESSION['authenticatedUser'])) { ?>

		<h1>Please enter your credentials bellow</h1>

		<form id="login" method="POST" action="checkingId.php">

			<label for="id">ID</label>
			<br />
			<input type="text" name="id" id="id" />
			<br /><br />

			<?php 
			if (isset($_GET['formError'])) { // in case of the credentials don't match anything!
				echo "<p><em>The credentials are not the right ones, an alert has been sent to the security team....</em></p>";
				echo "<br />";
			}
			?>

			<input type="submit" value="Next" name="loginId" />
		</form>

		<br />

		<div>
			<p>Register <a href="register.php">here</a>.</p>
		</div>
	
	<?php } else { // authenticated users don't have to login
		header("Location: .");
	} ?>

	</main>
</body>

</html>